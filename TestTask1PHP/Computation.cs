﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask1PHP
{
    public abstract class Computation<T> : IIncrementable<T> where T : IComparable
    {
        private T _min;
        private T _max;
        public T Min
        {
            get
            {
                if (!_isProcessedAtLeastOneitem)
                {
                    throw new ZeroItemsProcessedException();
                }
                return _min;
            }
            private set
            {
                _min = value;
            }
        }

        public T Max
        {
            get
            {
                if (!_isProcessedAtLeastOneitem)
                {
                    throw new ZeroItemsProcessedException();
                }

                return _max;
            }
            private set
            {
                _max = value;
            }
        }

        public Dictionary<T, int> Duplicates { get; private set; }

        private bool _isProcessedAtLeastOneitem = false;

        public Computation()
        {
            Duplicates = new Dictionary<T, int>();
        }

        public void ProcessItem(T currentItem)
        {
            if (!_isProcessedAtLeastOneitem)
            {
                Min = currentItem;
                Max = currentItem;
                Duplicates[currentItem] = 0;

                _isProcessedAtLeastOneitem = true;

            }
            else
            {
                UpdateDuplicates(currentItem);
                UpdateMin(currentItem);
                UpdateMax(currentItem);
            }
        }

        private void UpdateMax(T currentItem)
        {
            if (this.Max.CompareTo(currentItem) < 0)
            {
                this.Max = currentItem;
            }
        }

        private void UpdateMin(T currentItem)
        {
            if (this.Min.CompareTo(currentItem) > 0)
            {
                this.Min = currentItem;
            }
        }

        private void UpdateDuplicates(T currentItem)
        {
            if (this.Duplicates.ContainsKey(currentItem))
            {
                ++this.Duplicates[currentItem];
            }
            else
            {
                this.Duplicates.Add(currentItem, 1);
            }
        }

        public IEnumerable<T> GetMissingItems()
        {
            for (T i = this.Min; i.CompareTo(this.Max) < 0; i = Increment(i))
            {
                if (!this.Duplicates.ContainsKey(i))
                {
                    yield return i;
                }
            }
        }

        public abstract T Increment(T oldVal);
    }
}

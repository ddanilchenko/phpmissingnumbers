﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask1PHP
{
    public interface IIncrementable<T>
    {
        T Increment(T oldVal);
    }
}

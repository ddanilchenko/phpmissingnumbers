﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask1PHP
{
    public class IntComputation : Computation<int>
    {
        public override int Increment(int oldVal)
        {
            return ++oldVal;
        }
    }
}

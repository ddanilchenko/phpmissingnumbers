﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask1PHP;

namespace TestTask1PHP
{

    class Program
    {
        private static IntComputation doEverything(int[] data)
        {
            if (data == null || data.Length == 0)
            {
                return null;
            }
            
            int itm = data[0];

            IntComputation result = new IntComputation();
                       
            for (int i = 1; i<data.Length; ++i)
            {
                result.ProcessItem(data[i]);

            }

            return result;
        }


        static void DisplayComputations(IntComputation result)
        {
            if (result == null)
            {
                throw new ResultNotInitializedException();
            }

            Console.WriteLine("Range is {0} to {1}", result.Min, result.Max);
            Console.WriteLine("Missing numbers:");
            foreach (var i in result.GetMissingItems())
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("Duplicate numbers:");

            foreach (var k in result.Duplicates.Keys)
            {
                int cnt = result.Duplicates[k];
                if (cnt <= 1)
                {
                    continue;
                }
                Console.WriteLine("{0} appears {1} times", k, cnt);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                IntComputation computation = doEverything(new int[] { 3, 1, -5, 3, 3, -5, 0, 10, 1, 1 });
                DisplayComputations(computation);

                computation = doEverything(new int[] { 1, -100000, 100000 });
                DisplayComputations(computation);

            }
            catch (ResultNotInitializedException)
            {
                Console.WriteLine("Result is null. Nothing to display");
            }
            catch(TestTaskException)
            {
                Console.WriteLine("Something Went wrong during computation");
            }
            catch(Exception )
            {
                Console.WriteLine("Unhandled exception");
            }
        }
    }
}
